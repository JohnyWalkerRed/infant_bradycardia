pOrder = 10; N = 1; receptiveField = 33;

for subjIdx = 1:10
    
    data = load(['/Users/alg/Documents/MATLAB/brady_events/infant' num2str(subjIdx) '_RR.txt']);
    
    nChunks = floor(length(data(:,2))/receptiveField);
    rmse = []; mase = [];     NRmse = []; NMase = [];

    for ii = 1:nChunks-1
        
        dataSegment = data((receptiveField*(ii-1))+1:(receptiveField*ii),2);
        testPt = data((receptiveField*ii)+1,2);
        ar_coeffs = aryule(dataSegment,pOrder);
        nextValue = -ar_coeffs(2:end) * dataSegment(end:-1:end-pOrder+1);
        
        rmse(ii) = sqrt(sum((testPt- nextValue).^2));
        rmse_trivial(ii) = sqrt(sum((testPt- dataSegment(end)).^2));

        if rmse_trivial(ii) == 0
           rmse_trivial(ii) = 1e-4; 
        end
        
        mase(ii) = rmse(ii)./rmse_trivial(ii);
        Ntrivial_pt = nextValue;
        % predict n steps ahead
        lastSegment =  cat(1,data((receptiveField*(ii-1))+2:(receptiveField*ii)+1,2), nextValue);
        for jj = 1:N
            ar_coeffs = aryule(lastSegment,pOrder);
            nextValue = -ar_coeffs(2:end) * lastSegment(end:-1:end-pOrder+1);
            lastSegment = cat(1, lastSegment(2:end), nextValue);
        end
        
        NtestPt =  data((receptiveField*ii)+(1:N),2);
        NRmse(ii) = sqrt(sum((NtestPt- lastSegment(end:-1:end-N+1)).^2)/N);
        NRmse_trivial(ii) = sqrt(sum((NtestPt - Ntrivial_pt).^2)/N);
        NMase(ii) = NRmse(ii)/NRmse_trivial(ii);

        1;
    end
    
    meanRMSE(subjIdx) = nanmean(rmse);
    meanMASE(subjIdx) = nanmean(mase);
    
    NmeanRMSE(subjIdx) = nanmean(NRmse);
    NmeanMASE(subjIdx) = nanmean(NMase);
    1;

end
    
     figure; bar(NmeanRMSE)
figure; bar(NmeanMASE)
1;