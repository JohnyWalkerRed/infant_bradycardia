import sys
sys.path.append('..')
from WaveNet.WaveNet2 import WaveNet2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

bradycardia_data = './Data/RR/infant1_RR.txt'

if __name__ == '__main__':
    X = pd.read_csv(bradycardia_data, sep=' ')
    X = X.as_matrix()
    X = np.expand_dims(X[:, 1], 1)
    wavenet2 = WaveNet2(initial_filter_width=30, 
                        filter_width=5, 
                        dilation_channels=32, 
                        dilations=[1, 2, 4, 8],
                        forecast_horizon=90,
                        random_seed=22943)
    predicted, actual = wavenet2.train(X, epochs=500)
    rmse = np.sqrt(np.mean(np.square(predicted - actual)))
    print rmse
    plt.plot(predicted, label='predicted')
    plt.plot(actual, label='actual')
    plt.legend()
    plt.show()